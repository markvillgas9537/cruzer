import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    public authService: AuthService,
    private _router: Router
  ) {
    
  }

  ngOnInit(): void {
    this._router.events
    .subscribe(event => {
      setTimeout(() => {
        if (this._router.url.includes('login') && this.authService.loggedIn()) {
          this._router.navigate(['home']);
        } 
      }, 0);
    });
  }

  logout() {
    localStorage.removeItem('accessToken');
    this._router.navigate(['login']);
  }
}
