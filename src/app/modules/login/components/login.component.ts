import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _loginService: LoginService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._initForm();
  }

  submit() {
    const reqBody = {
      username: this.loginForm.controls['username'].value,
      password: this.loginForm.controls['password'].value
    };

    this._loginService.login(reqBody)
      .subscribe({
        next: (result) => {
          alert('Successfully logged in.');
          localStorage.setItem('accessToken', result.accessToken);
          this._router.navigate(['/home']);
        }, 
        error: (err) => {
          console.log(err);
        }, 
        complete: () => {

        }
      })
  }

  private _initForm() {
    this.loginForm = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
}
