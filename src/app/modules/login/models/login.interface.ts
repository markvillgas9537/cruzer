export interface UserAccount {
    username: string;
    password: string;
}

export interface Token {
    accessToken: string;
    refreshToken: string;
}