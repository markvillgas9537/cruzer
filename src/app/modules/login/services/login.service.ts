import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Token, UserAccount } from '../models/login.interface';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  apiUrl: string = 'http://localhost:3000/auth';

  constructor(
    private http: HttpClient
  ) { }

  login(userAccount: UserAccount) {
    return this.http.post<Token>(`${this.apiUrl}/login`, userAccount);
  }
}
