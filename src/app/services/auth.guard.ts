import { Injectable } from "@angular/core";
import { AuthService } from "./auth.services";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthGuard {
    constructor(
        private auth: AuthService, 
        private router: Router
    ) {}

    canActivate(): any {
        if (this.auth.loggedIn()) {
            return true;
            
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}