import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    public loggedIn(): boolean {
        return !!localStorage.getItem('accessToken');
    }

    public getToken(): any {
        return localStorage.getItem('accessToken');
    }

    public logout(): void {
        localStorage.removeItem('accessToken');
    }
}